package dk.scott.tiger.observer;

import dk.scott.tiger.Application;
import dk.scott.tiger.TrafficLight;
import dk.scott.tiger.dao.LoginBean;
import dk.scott.tiger.log.LogFrame;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.prefs.Preferences;

public class UpdateWorker  implements Runnable {
  Preferences prefs = Preferences.userNodeForPackage(dk.scott.tiger.Desktop.class);
    private LogFrame logFrame;
    private JLabel jLabelFooterState;
    private TrafficLight trafficLight;
    private LoginBean loginBean;
    private Boolean aBoolean;
    private int dbId;
    final String FIRST_UPDATE = "firstStopUpdate";
    final String SECOND_UPDATE = "secondStopUpdate";
    final static Logger log = Logger.getLogger(Application.class);


    public UpdateWorker(LoginBean firstTns, int  _dbId,LogFrame _logFrame,TrafficLight _trafficLight,JLabel _jLabelFooterState) {

        this.loginBean = firstTns;
        this.dbId=_dbId;
        this.logFrame=_logFrame;
        this.trafficLight=_trafficLight;
        this.jLabelFooterState=_jLabelFooterState;
    }

    protected void done(){
        try
        {
            Date date = new Date(System.currentTimeMillis());
            jLabelFooterState.setText("Last update: " + date );
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        log.debug("Start updating from auto " + trafficLight.getName() );
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        StatusWorker statusWorker = new StatusWorker();
        trafficLight.setCurrentLightState(TrafficLight.STATE_YELLOW);
        if(dbId==1)
            aBoolean = prefs.getBoolean(FIRST_UPDATE,false);
        else
            aBoolean = prefs.getBoolean(SECOND_UPDATE,false);

        if(aBoolean== false){
            jLabelFooterState.setText("Updating");
            logFrame.getData(loginBean);
            logFrame.jTable1.revalidate();
            logFrame.jTable1.repaint();

        }
        statusWorker.doUpdate(loginBean, trafficLight);
        log.debug("Update finnished " + trafficLight.getName() );
        done();
    }
}
