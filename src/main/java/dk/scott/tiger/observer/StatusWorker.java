package dk.scott.tiger.observer;

import dk.scott.tiger.TrafficLight;
import dk.scott.tiger.dao.LoginBean;
import dk.scott.tiger.transfer_global_value.TransferGlobalValue;
import dk.scott.tiger.transfer_global_value.TransferRepository;

import java.sql.SQLException;

public class StatusWorker {
    public void doUpdate(LoginBean loginBean, TrafficLight trafficLight){
        // Traffic light
        TransferRepository transferRepository =new TransferRepository();
        try {
            TransferGlobalValue transferGlobalValueFirst = transferRepository.find(1, loginBean);
            Long x = transferGlobalValueFirst.getX();

            if(x==1)
                trafficLight.setCurrentLightState(TrafficLight.STATE_RED);
            else
                trafficLight.setCurrentLightState(TrafficLight.STATE_GREEN);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
