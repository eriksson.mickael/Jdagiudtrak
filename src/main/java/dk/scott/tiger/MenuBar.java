/*
 * The MIT License
 *
 * Copyright 2017 Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dk.scott.tiger;

import dk.scott.tiger.config.I18N;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * MenuBar class.
 *
 * @author Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn)
 */
public class MenuBar extends JMenuBar
{
    final static Logger log = Logger.getLogger(MenuBar.class);

    // file :
    final JMenu jMenuFile = new JMenu(I18N.lang("menubar.jMenuFile"));
    final JMenuItem jMenuItemLoginFirst = new JMenuItem(I18N.lang("menubar.jMenuOMKFrame1"));
    final JMenuItem jMenuItemLoginSecond = new JMenuItem(I18N.lang("menubar.jMenuOMKFrame2"));
    final JMenuItem jMenuItemLogFrame1 = new JMenuItem(I18N.lang("menubar.jMenuItemLogFrame"));
    final JMenuItem jMenuItemLogFrame2 = new JMenuItem(I18N.lang("menubar.jMenuItemLogFrame2"));
    final JMenuItem jMenuItemQuit = new JMenuItem(I18N.lang("menubar.jMenuItemQuit"));
    // help :
    final JMenu jMenuHelp = new JMenu(I18N.lang("menubar.jMenuHelp"));
    final JMenuItem jMenuItemFrameAbout = new JMenuItem(I18N.lang("menubar.jMenuItemFrameAbout"));

    /**
     * Constructor.
     */
    public MenuBar() {
        log.debug("START constructor...");

        // file :
        add(jMenuFile);
        jMenuFile.setMnemonic(KeyEvent.VK_F);

        jMenuItemLoginFirst.setAccelerator(KeyStroke.getKeyStroke('R', Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
        jMenuFile.add(jMenuItemLoginFirst);
        jMenuFile.add(jMenuItemLoginSecond);


        jMenuFile.addSeparator();

        jMenuItemLogFrame1.setAccelerator(KeyStroke.getKeyStroke('P', Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
        jMenuFile.add(jMenuItemLogFrame1);
        jMenuFile.add(jMenuItemLogFrame2);

        jMenuFile.addSeparator();

        jMenuItemQuit.setAccelerator(KeyStroke.getKeyStroke('Q', Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
        jMenuFile.add(jMenuItemQuit);

        // help :
        add(jMenuHelp);
        jMenuHelp.setMnemonic(KeyEvent.VK_H);

        jMenuItemFrameAbout.setAccelerator(KeyStroke.getKeyStroke('A', Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
        jMenuHelp.add(jMenuItemFrameAbout);

        log.debug("End of constructor.");
    }
}
