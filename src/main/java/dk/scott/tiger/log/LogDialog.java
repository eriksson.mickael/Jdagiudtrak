/*
 * The MIT License
 *
 * Copyright 2017 Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dk.scott.tiger.log;

import com.github.lgooddatepicker.components.DateTimePicker;
import dk.scott.tiger.config.I18N;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Random;

public class LogDialog extends JDialog
{
    final static Logger log = Logger.getLogger(LogDialog.class);

    final JLabel jLabelTid = new JLabel(I18N.lang("logframe.jLabeltId"));
    final JLabel jLabelReg = new JLabel(I18N.lang("logframe.jlabelregistrering_id"));
    final JLabel jLabelFeature = new JLabel(I18N.lang("logframe.jlabelfeature"));
    final JLabel jLabelType = new JLabel(I18N.lang("logframe.jlabeltype"));
    JLabel jLabelTekst = new JLabel(I18N.lang("logframe.jlabeltekst"));

    final DateTimePicker jTextFieldTid = new DateTimePicker();
    final JTextField jTextFieldReg = new JTextField(40);
    final JTextField jTextFieldFeature = new JTextField(40);
    final JTextField jTextType = new JTextField(40);
    final JTextField jTextFieldTekst = new JTextField(40);

    final JButton jButtonSave = new JButton(I18N.lang("logframe.jButtonSave"));
    final JButton jButtonCancel = new JButton(I18N.lang("logframe.jButtonCancel"));

   final boolean isNew;
    final LogBean logToEdit;

    /**
     * Constructor to call super first and then init isNew and logToEdit
     * attributes.
     *
     * @param owner
     * @param title
     * @param modal
     * @param isNew
     * @param logToEdit
     */
    public LogDialog(Frame owner, String title, boolean modal, boolean isNew, LogBean logToEdit)
    {
        super(owner, title, modal);

        log.debug("START constructor...");

        // set this param globals to use in other methods :
        this.isNew = isNew;
        this.logToEdit = logToEdit;

        setLocation(new Random().nextInt(150), new Random().nextInt(150));
        setSize(350, 200);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // new :
        if (isNew)
        {
            setLayout(new GridLayout(4, 2));
        } // modification :
        else
        {
            setLayout(new GridLayout(5, 2));
            getContentPane().add(jLabelTid);
            getContentPane().add(jTextFieldTid);

            // write values in form :
            jTextFieldTid.datePicker.setDate(LocalDate.from(logToEdit.getTid().toLocalDateTime()));
            jTextFieldReg.setText(logToEdit.getRegistreringId().toString());
            jTextFieldFeature.setText( logToEdit.getFeature());
            jTextType.setText(logToEdit.getType());
            jTextFieldTekst.setText(logToEdit.getTekst());
        }

        getContentPane().add(jLabelReg);
        getContentPane().add(jTextFieldReg);

        getContentPane().add(jLabelFeature);
        getContentPane().add(jTextFieldFeature);

        getContentPane().add(jLabelType);
        getContentPane().add(jTextFieldTekst);

        getContentPane().add(jButtonSave);
        jButtonSave.addActionListener((ActionEvent ev) ->
        {
            jButtonSaveActionPerformed(ev);
        });

        getContentPane().add(jButtonCancel);
        jButtonCancel.addActionListener((ActionEvent ev) ->
        {
            jButtonCancelActionPerformed(ev);
        });

        log.debug("End of constructor.");

        setVisible(true);
    }

    public void jButtonSaveActionPerformed(ActionEvent ev)
    {
        log.debug("ActionEvent on " + ev.getActionCommand());

        LogRepository productRepository = new LogRepository();
        LogBean logBean = new LogBean();

        logBean.setTid(Timestamp.valueOf(String.valueOf(jTextFieldTid.datePicker.getDate())));
        logBean.setRegistreringId(Long.parseLong(jTextFieldReg.getText()));
        logBean.setFeature(jTextFieldFeature.getText());
        logBean.setTekst(jTextFieldTekst.getText());

            logBean.setTid(this.logToEdit.getTid());

    }

    public void jButtonCancelActionPerformed(ActionEvent ev)
    {
        log.debug("ActionEvent on " + ev.getActionCommand());

        this.dispose();
    }

}
