/*
 * The MIT License
 *
 * Copyright 2017 Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dk.scott.tiger.log;

import dk.scott.tiger.dao.DAOConnection;
import dk.scott.tiger.dao.LoginBean;
import dk.scott.tiger.dao.Repository;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * The Log repository implementation.
 *
 * @author Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn)
 */
public class LogRepository implements Repository<LogBean>
{
    final static Logger log = Logger.getLogger(LogRepository.class);

    @Override
    public LogBean find(long id,LoginBean tns) {
        log.debug("Start method...");

        LogBean obj = null;


        try
        {
            PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement(
                    "SELECT * FROM log WHERE id=?");

            prepared.setLong(1, id);

            ResultSet result = prepared.executeQuery();

            if (result.first())
            {
                obj = map(result);
            }
            prepared.close();
            prepared=null;
        } catch (SQLException e)
        {
            log.error("Error finding product : " + e);
        }

        log.debug("End method.");

        return obj;
    }

    @Override
    public LogBean find(Timestamp id,LoginBean tns) {
        log.debug("Start method...");
        LogBean obj = null;
        try
        {
            PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement(
                    "SELECT * FROM log WHERE tid=?");

            prepared.setTimestamp(1, id);
            ResultSet result = prepared.executeQuery();

            if (result.first())
            {
                obj = map(result);
            }
               prepared.close();
            prepared=null;
        } catch (SQLException e)
        {
            log.error("Error finding product : " + e);
        }

        log.debug("End method.");

        return obj;
    }

    /**
     * Find all items.
     *
     * @return
     */
    @Override
    public ArrayList<LogBean> findAll(LoginBean tns)
    {
        log.debug("Start method...");

        ArrayList<LogBean> products = new ArrayList<>();
        try {
            PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement(
                    "SELECT * FROM log order by tid desc");

            ResultSet result = prepared.executeQuery();

            while (result.next())
            {
                products.add(map(result));
            }
            prepared.close();
            prepared=null;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        log.debug("End method.");
        return products;
    }
    @Override
    public ArrayList<LogBean> findFirst(LoginBean tns, int count)
    {
        log.debug("Start method...");

        ArrayList<LogBean> products = new ArrayList<>();

        try
        {
            log.debug("Preparing...");
            PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement(
                    "SELECT * FROM log order by tid desc fetch first " + count +" rows only ");

            ResultSet result = prepared.executeQuery();

            while (result.next())
            {
                products.add(map(result));
            }
          prepared.close();
            prepared=null;
        } catch (SQLException e)
        {
            log.error("Error finding products : " + e);
        }

        log.debug("End method.");

        return products;
    }
    /**
     * Map the current row of the given ResultSet to an Object.
     *
     * @param resultSet
     * @return The mapped Object from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static LogBean map(ResultSet resultSet) throws SQLException
    {
        LogBean obj = new LogBean();
        obj.setTid(resultSet.getTimestamp("TID"));
        obj.setRegistreringId(resultSet.getLong("REGISTRERING_ID"));
        obj.setFeature(resultSet.getString("FEATURE"));
        obj.setType(resultSet.getString("TYPE"));
        obj.setTekst(resultSet.getString("TEKST"));


        return obj;
    }
}
