package dk.scott.tiger.log;


public class LogDTO {
   private java.sql.Timestamp tid;
   private Long registreringId;

   private String feature;
    private String type;
   private String tekst;

   public java.sql.Timestamp getTid() {
        return this.tid;
    }

   public void setTid(java.sql.Timestamp tid) {
       this.tid = tid;
    }
    public Long getRegistreringId() {
       return this.registreringId;
    }


    public void setRegistreringId(Long registreringId) {
       this.registreringId = registreringId;

    }


   public String getFeature() {

        return this.feature;
    }


  public void setFeature(String feature) {

        this.feature = feature;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTekst() {
        return this.tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }
}
