package dk.scott.tiger.log;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LOG")
public class LogBean {
    @Id
    @Column(name = "TID")
    private java.sql.Timestamp tid;

    @Column(name = "REGISTRERING_ID")
    private Long registreringId;

    @Column(name = "FEATURE")
    private String feature;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "TEKST")
    private String tekst;


    public java.sql.Timestamp getTid() {
        return this.tid;
    }

    public void setTid(java.sql.Timestamp tid) {
        this.tid = tid;
    }

    public Long getRegistreringId() {
        return this.registreringId;
    }

    public void setRegistreringId(Long registreringId) {
        this.registreringId = registreringId;
    }

    public String getFeature() {
        return this.feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTekst() {
        return this.tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }
}
