/*
 * The MIT License
 *
 * Copyright 2017 Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dk.scott.tiger;

import dk.scott.tiger.config.I18N;
import dk.scott.tiger.dao.Jlogin;
import dk.scott.tiger.dao.LoginBean;
import dk.scott.tiger.dao.TNSUtil;
import dk.scott.tiger.log.LogFrame;
import dk.scott.tiger.observer.UpdateWorker;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

/**
 * Desktop class.
 *
 */
public class Desktop extends JFrame
{

    final static Logger log = Logger.getLogger(Desktop.class);
    private static final String tns =  System.getenv("TNS_ADMIN") + "/tnsnames.ora";
    final Preferences prefs = Preferences.userNodeForPackage(dk.scott.tiger.Desktop.class);
    final JDesktopPane jDesktopPane = new JDesktopPane();
    final JPanel jPanel = new JPanel();
    private JComboBox jComboBoxTimer ;
    final VerticalTrafficLight trafficLightFirst = new VerticalTrafficLight();
    final VerticalTrafficLight trafficLightSecond = new VerticalTrafficLight();
    final JLabel jLabelFooterState = new JLabel(I18N.lang("desktop.jLabelFooterState") + System.getProperty("os.name"));
    final TNSUtil tnsUtil = new TNSUtil(tns);;
    // internal frames :
    final FrameAbout frameAbout = new FrameAbout();
    final LogFrame logFrame = new LogFrame(1);
    final LogFrame logFrame2 = new LogFrame(2);
    private LoginBean firstLoginBean;
    private LoginBean secondLoginBean;
    private int desktopWidth;
    private int desktopHeight;
    final String UPDATE_INTERVAL = "updateIntervall";
    final int defautInt=60000;
    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    // menu :
    final MenuBar menuBar = new MenuBar();
    private UpdateWorker updateWorker;
    private UpdateWorker updateWorker2;


    /**
     * Constructor.
     */
    public Desktop() {
        log.debug("START constructor...");
        try {
        // init frame :
        setTitle(I18N.lang("desktop.title"));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        desktopWidth = screenSize.width/2 + 100;
        desktopHeight = screenSize.height/2 +150 ;
        setBounds(0, 0, desktopWidth, desktopHeight);
        firstLoginBean= tnsUtil.getLogin(1);
        secondLoginBean = tnsUtil.getLogin(2);
        // init desktop :
        getContentPane().add(jDesktopPane, BorderLayout.CENTER);
        jPanel.setLayout(new GridBagLayout());

       //Constraints
        GridBagConstraints c = new GridBagConstraints();
        // Common constraints.
        c.ipadx=0;
        c.fill= GridBagConstraints.HORIZONTAL;
        // First trafficLight
        trafficLightFirst.setCurrentLightState(TrafficLight.STATE_GREEN);
        trafficLightFirst.setRotation(1);
        trafficLightFirst.setAlignmentX(1);
        trafficLightFirst.setBorder(BorderFactory.createLineBorder(Color.black));
        trafficLightFirst.setAlignmentX(RIGHT_ALIGNMENT);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0,0,0,40);
        c.gridx = 0;
        c.gridy = 0;
        //Add to panel
        jPanel.add(trafficLightFirst,c);
        JSeparator separate = new JSeparator(SwingConstants.HORIZONTAL);
        jPanel.add(separate);

        // Second trafficLight
        trafficLightSecond.setCurrentLightState(TrafficLight.STATE_GREEN);
            trafficLightSecond.setName("Second");
        trafficLightSecond.setRotation(1);
        trafficLightSecond.setBorder(BorderFactory.createLineBorder(Color.black));

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0;
        c.gridx = 1;
        c.gridy = 0;
        //Add to panel

        jPanel.add(trafficLightSecond,c);
        // Add panel to desktop.
        getContentPane().add(jPanel, BorderLayout.NORTH,1);

        getContentPane().add(jLabelFooterState, BorderLayout.SOUTH);
        jDesktopPane.setLayout(new FlowLayout(FlowLayout.LEFT));

        // add internal frames to desktop :
        jDesktopPane.add(frameAbout);
        //
        Dimension dimension = new Dimension(500,500);
        logFrame.setPreferredSize(dimension);
        logFrame2.setPreferredSize(dimension);
        jDesktopPane.add(logFrame);
        jDesktopPane.add(logFrame2);
        //
        logFrame.setVisible(true);
        logFrame2.setVisible(true);

        // add the menu bar :
        String[] strings = {"1 sek","10 sek","1 minut","2 minuter"};
        Vector model = new Vector();
        model.addElement(new Intervalls(Constants.oneSecond, I18N.lang("intervall.oneSec")));
        model.addElement(new Intervalls(Constants.twoSeconds, I18N.lang("intervall.teneSec")));
        model.addElement(new Intervalls(Constants.oneMinute, I18N.lang("intervall.oneMin")));
        model.addElement(new Intervalls(Constants.twoMinutes, I18N.lang("intervall.twoMin")));

        jComboBoxTimer= new JComboBox(model);
        jComboBoxTimer.setRenderer(new ItemRenderer());

        jComboBoxTimer.setSelectedItem(model.contains(prefs.getInt(UPDATE_INTERVAL,0)));
        menuBar.add(Box.createRigidArea(new Dimension(5,0)));
        menuBar.add(new JLabel(I18N.lang("menubar.frequency")));
        menuBar.add(Box.createRigidArea(new Dimension(5,0)));
        menuBar.add(jComboBoxTimer);

        Dimension minSize = new Dimension(5, menuBar.getHeight());
        Dimension prefSize = new Dimension(5,  menuBar.getHeight());
        Dimension maxSize = new Dimension(Short.MAX_VALUE,  menuBar.getHeight());
        menuBar.add(new Box.Filler(minSize, prefSize, maxSize));
        setJMenuBar(menuBar);

        // menu  :

        // schedule the worker at an intervall of 5 seconds
            UpdateWorker  updateWorkers = new UpdateWorker(firstLoginBean,1,logFrame,trafficLightFirst,jLabelFooterState);
            UpdateWorker  updateWorkers2 = new UpdateWorker(secondLoginBean,1,logFrame,trafficLightSecond,jLabelFooterState);
            int anInt = prefs.getInt(UPDATE_INTERVAL, 60000);
            scheduler.scheduleAtFixedRate(updateWorkers, 10000, anInt, TimeUnit.MILLISECONDS);
            scheduler.scheduleAtFixedRate(updateWorkers2, 10000, anInt, TimeUnit.MILLISECONDS);

        // Timer combobox:
        jComboBoxTimer.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                log.debug("ActionEvent on " + actionEvent.getActionCommand());
             //   "1 sek","10 sek","1 minut"}
                Intervalls str = (Intervalls)jComboBoxTimer.getSelectedItem();

                        prefs.putInt(UPDATE_INTERVAL,str.getId());
                        if(scheduler.isShutdown() == false){
                            scheduler.shutdown();
                        } ;
                scheduler.scheduleAtFixedRate(updateWorkers, 10000, str.getId(), TimeUnit.MILLISECONDS);
                scheduler.scheduleAtFixedRate(updateWorkers2, 10000, str.getId(), TimeUnit.MILLISECONDS);

            }//
        });
        // jMenuItemQuit :
        menuBar.jMenuItemQuit.addActionListener((ActionEvent ev) ->
        {
            log.debug("ActionEvent on " + ev.getActionCommand());

            if (confirmBeforeExit())
            {
                System.exit(0);
            }
        });
        //timer
        // jMenuItemFrameAbout :
        menuBar.jMenuItemFrameAbout.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ev)
            {
                log.debug("ActionEvent on " + ev.getActionCommand());

                frameAbout.setVisible(true);
            }
        });

        // jMenuItemFrame1 :
        menuBar.jMenuItemLoginFirst.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ev)
            {
                LoginBean loginBean = new LoginBean().get(1);
                Jlogin jlogin = new Jlogin(loginBean);
                log.debug("ActionEvent on " + ev.getActionCommand());
            }
        });
        menuBar.jMenuItemLoginSecond.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ev)
            {
                LoginBean loginBean = new LoginBean().get(2);
                Jlogin jlogin = new Jlogin(loginBean);
                log.debug("ActionEvent on " + ev.getActionCommand());
            }
        });
        // window closing event :
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent ev)
            {
                log.debug("WindowEvent on " + ev.paramString());

                if (confirmBeforeExit())
                {
                    System.exit(0);
                }
            }
        });

        // logFrame :
        menuBar.jMenuItemLogFrame1.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ev)
            {
                log.debug("ActionEvent on " + ev.getActionCommand());

                // refresh the Table Model :
                logFrame.jTable1.setModel(logFrame.getData(firstLoginBean));
                logFrame.setVisible(true);
            }
        });

        // logFrame :
    menuBar.jMenuItemLogFrame2.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent ev)
        {
            log.debug("ActionEvent on " + ev.getActionCommand());
            // refresh the Table Model :
            logFrame2.jTable1.setModel(logFrame.getData(secondLoginBean));
            logFrame2.setVisible(true);
        }
    });

        setVisible(true);
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
        log.debug("End of constructor.");
    }
    /**
     * Show confirm dialog before closing the window.
     *
     * @return boolean true user answer Yes.
     */
    public boolean confirmBeforeExit()
    {
        log.debug("Display confirm dialog...");

        if (JOptionPane.showConfirmDialog(this, I18N.lang("desktop.confirmbeforeexitdialog.text"), I18N.lang("desktop.confirmbeforeexitdialog.title"), JOptionPane.YES_NO_OPTION) == 0)
        {
            log.debug("User answer YES.");
            scheduler.shutdown();
            return true;
        }

        log.debug("User answer NO.");
        return false;
    }
}
