package dk.scott.tiger.tbp;

import dk.scott.tiger.dao.DAOConnection;
import dk.scott.tiger.dao.LoginBean;
import dk.scott.tiger.dao.Repository;
import dk.scott.tiger.log.LogRepository;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class TpbRepository implements Repository<TpbLogBean> {
    final static Logger log = Logger.getLogger(LogRepository.class);
    @Override
    public TpbLogBean find(long id, LoginBean tns) throws SQLException {
        log.debug("Start method...");

        TpbLogBean tpbLogBean = null;
        try
        {
            PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement(
                    "SELECT * FROM tpb_log WHERE id=?");
            prepared.setLong(1, id);
            ResultSet result = prepared.executeQuery();
            if (result.first())
            {
                tpbLogBean = map(result);
            }
            prepared.close();
            prepared=null;
        } catch (SQLException e)
        {
            log.error("Error finding product : " + e);
        }

        log.debug("End method.");

        return tpbLogBean;
    }

    @Override
    public TpbLogBean find(Timestamp id, LoginBean tns) {
        return null;
    }

    @Override
    public ArrayList<TpbLogBean> findAll(LoginBean tns) {
        return null;
    }

    @Override
    public ArrayList<TpbLogBean> findFirst(LoginBean tns, int count) {
        log.debug("Start method...");
        ArrayList<TpbLogBean> products = new ArrayList<>();
        try
        {
            log.debug("Preparing...");
            PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement(
                    "SELECT * FROM tpb_log order by tid desc fetch first " + count + " rows only");

            ResultSet result = prepared.executeQuery();

            while (result.next())
            {
                products.add(map(result));
            }
            prepared.close();
            prepared=null;
        } catch (SQLException e)
        {
            log.error("Error finding products : " + e);
        }

        log.debug("End method.");

        return products;
    }

    public ArrayList<TpbLogBean> findFirstTen(LoginBean tns)
    {
        log.debug("Start method...");

        ArrayList<TpbLogBean> products = new ArrayList<>();

        try
        {
            log.debug("Preparing...");
            PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement(
                    "SELECT * FROM tpb_log order by tid desc fetch first 21 rows only");

            ResultSet result = prepared.executeQuery();
            while (result.next())
            {
                products.add(map(result));
            }
            prepared.close();
            prepared=null;
        } catch (SQLException e)
        {
            log.error("Error finding products : " + e);
        }

        log.debug("End method.");

        return products;
    }

    /***
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private static TpbLogBean map(ResultSet resultSet) throws SQLException
    {
        TpbLogBean tpbLogBean = new TpbLogBean();
        tpbLogBean.setTid(resultSet.getTimestamp("TID"));
        tpbLogBean.setKlasse(resultSet.getString("KLASSE"));
        tpbLogBean.setMessage(resultSet.getString("MESSAGE"));
        tpbLogBean.setId(resultSet.getLong("ID"));
        return tpbLogBean;
    }
}
