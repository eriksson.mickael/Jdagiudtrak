package dk.scott.tiger.tbp;

import javax.persistence.*;

@Entity
@Table(name = "TPB_LOG")
public class TpbLogBean {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "TID")
    private java.sql.Timestamp tid;

    @Column(name = "KLASSE")
    private String klasse;

    @Column(name = "MESSAGE")
    private String message;


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.sql.Timestamp getTid() {
        return this.tid;
    }

    public void setTid(java.sql.Timestamp tid) {
        this.tid = tid;
    }

    public String getKlasse() {
        return this.klasse;
    }

    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
