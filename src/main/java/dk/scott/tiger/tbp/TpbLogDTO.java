package dk.scott.tiger.tbp;

public class TpbLogDTO {
    private Long id;
    private java.sql.Timestamp tid;
    private String klasse;
    private String message;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.sql.Timestamp getTid() {
        return this.tid;
    }

    public void setTid(java.sql.Timestamp tid) {
        this.tid = tid;
    }

    public String getKlasse() {
        return this.klasse;
    }

    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
