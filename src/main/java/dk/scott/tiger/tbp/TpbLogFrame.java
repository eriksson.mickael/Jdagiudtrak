/*
 * The MIT License
 *
 * Copyright 2017 Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dk.scott.tiger.tbp;

import dk.scott.tiger.config.I18N;
import dk.scott.tiger.dao.Jlogin;
import dk.scott.tiger.dao.LoginBean;
import dk.scott.tiger.dao.TNSUtil;
import dk.scott.tiger.log.LogBean;
import dk.scott.tiger.log.LogRepository;
import dk.scott.tiger.observer.UpdateWorker;
import dk.scott.tiger.table.RendererHighlighted;
import dk.scott.tiger.table.RowFilterUtil;
import dk.scott.tiger.transfer_global_value.TransferRepository;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.prefs.Preferences;

/**
 **/
public class TpbLogFrame extends JInternalFrame
{
    final static Logger log = Logger.getLogger(TpbLogFrame.class);
    final JPanel jPanelHeader = new JPanel();
    final JButton jButtonRefresh = new JButton(I18N.lang("logform.jButtonRefresh"));
    final JButton jButtonReset = new JButton(I18N.lang("logform.jButtonReset"));
    final Preferences prefs = Preferences.userNodeForPackage(dk.scott.tiger.Desktop.class);
    private static final String tns =  System.getenv("TNS_ADMIN") + "/tnsnames.ora";
    final String FIRST_TNS = "firstTns";
    final String SEC_TNS = "secondTns";
    final String FIRST_USER = "firstUser";
    final String FIRST_PWD = "firstPwd";
    final String SEC_USER = "sectUser";
    final String SEC_PWD = "secPwd";
    final String FIRST_UPDATE = "firstStopUpdate";
    final String SECOND_UPDATE = "secondStopUpdate";
    final String defaultValue = "";
    private  UpdateWorker updateWorker;
    private JComboBox jComboBox;
    private JCheckBox jCheckBox;
    public  JTable jTable1;
    private LoginBean loginBean;
    private int dbId;

    /**
     *
     * Constructor.
     */
    public TpbLogFrame(int _dbId)  {
        log.debug("START constructor...");

        this.loginBean = new LoginBean();
        try {
            this.dbId = _dbId;
            if(_dbId == 1){
                setTitle(I18N.lang("logform.name"));
            this.loginBean.setTnsnames( prefs.get(FIRST_TNS, defaultValue));
            this.loginBean.setUserName( prefs.get(FIRST_USER, defaultValue));
            this.loginBean.setPwd( prefs.get(FIRST_PWD, defaultValue));
                this.loginBean.setDbId(_dbId);
                }
            else if(_dbId == 2) {
                    setTitle(I18N.lang("logform.name2"));
                this.loginBean.setTnsnames( prefs.get(SEC_TNS, defaultValue));
                this.loginBean.setUserName( prefs.get(SEC_USER, defaultValue));
                this.loginBean.setPwd( prefs.get(SEC_PWD, defaultValue));
                this.loginBean.setDbId(_dbId);
            }
            if( this.loginBean.getUserName().equalsIgnoreCase("")){
                Jlogin jlogin = new Jlogin(this.loginBean);
            }
            setLocation(new Random().nextInt(100), new Random().nextInt(100));
            setSize(800, 850);
            setVisible(false);
            setClosable(true);
            setIconifiable(true);
            setDefaultCloseOperation(HIDE_ON_CLOSE);
            TNSUtil tnsUtil = new TNSUtil(tns);
            jComboBox = new JComboBox();
            jComboBox.setModel(new DefaultComboBoxModel<>(tnsUtil.getTNSentries().toArray(new String[0])));
         //   jPanelHeader.add(jComboBox);
            jComboBox.setSelectedItem(FIRST_TNS);
            jComboBox.addActionListener(new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent actionEvent) {
                                                String selected = Objects.requireNonNull(jComboBox.getSelectedItem()).toString();
                                                prefs.put(FIRST_TNS,selected);

                                            }
                                        });
            jCheckBox = new JCheckBox("Stop update");
            if(dbId ==1)
            jCheckBox.setSelected(prefs.getBoolean(FIRST_UPDATE,false));
            else
                jCheckBox.setSelected(prefs.getBoolean(SECOND_UPDATE,false));
            jPanelHeader.add(jCheckBox);

            // header :
            jPanelHeader.setBorder(BorderFactory.createTitledBorder(I18N.lang("logform.jPanelHeader")));
            Dimension dim = new Dimension(100,100);
            jPanelHeader.add(jButtonRefresh);

            jCheckBox.addActionListener((ActionEvent ev) ->
            {
                jCheckBoxCheckActionPerformed(ev);
            });

            jButtonRefresh.addActionListener((ActionEvent ev) ->
            {
                jButtonUpdateActionPerformed(ev);
            });

            jPanelHeader.add(jButtonReset);
            jButtonReset.addActionListener((ActionEvent ev) ->
            {
                jButtonResetActionPerformed(ev);
            });

            getContentPane().add(jPanelHeader, BorderLayout.NORTH);

            // Table :
            //   jTable1 = new JTable();
            jTable1 = new JTable(this.getData(this.loginBean));
            jTable1.setAutoCreateRowSorter(true);
            jTable1.setDefaultEditor(Object.class, null);

            jTable1.getColumnModel().getColumn(0).setPreferredWidth(120);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(120);

            jTable1.getColumnModel().getColumn(1).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(50);

            jTable1.getColumnModel().getColumn(2).setPreferredWidth(70);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(70);

            jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
            jTable1.removeColumn(jTable1.getColumnModel().getColumn(3));

            getContentPane().add(new JScrollPane(jTable1), BorderLayout.CENTER);
            JTextField filterField = RowFilterUtil.createRowFilter(jTable1);
            RendererHighlighted renderer = new RendererHighlighted(filterField);

            jTable1.setDefaultRenderer(Object.class, renderer);
            jPanelHeader.add(filterField);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.debug("End of constructor.");
    }

    private void jCheckBoxCheckActionPerformed(ActionEvent ev) {
        log.debug("ActionEvent on " + ev.getActionCommand());
        if(jCheckBox.isSelected()){
            if(dbId==1)
                prefs.putBoolean(FIRST_UPDATE, true);
            else
                 prefs.putBoolean(SECOND_UPDATE,true);
        } else {
            if(dbId==1)
                prefs.putBoolean(FIRST_UPDATE, false);
            else
                prefs.putBoolean(SECOND_UPDATE,false);
        }

    }

    /**
     * Method to get the data from Repository and return it in DefaultTableModel
     * object. Very useful for refreshing JTable after data modification.
     *
     * @return
     */
    public DefaultTableModel getData(LoginBean tnsNames)  {
        log.debug("Start method...");
       // LoginForm loginDialog = new LoginForm(this.tnsNames.getTnsnames(),dbId);
        this.loginBean = tnsNames;
        DefaultTableModel model = null;
        try {
            // Comumns :
            String[] columns = new String[]
            {
                I18N.lang("logform.jtable1.column.tid"),
                I18N.lang("logform.jtable1.column.REGISTRERING_ID"),
                I18N.lang("logform.jtable1.column.FEATURE"),
                I18N.lang("logform.jtable1.column.TYPE"),
                I18N.lang("logform.jtable1.column.TEKST")
            };

            model = new DefaultTableModel(columns, 0);

            // get data rows :
            LogRepository logRepository = new LogRepository();
            ArrayList<LogBean> logs = logRepository.findFirst(this.loginBean,25);

            // transform ArrayList<> to Object[][] :
            for (LogBean log : logs)
            {
                model.addRow(new Object[]
                {
                        new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(log.getTid()),
                    log.getRegistreringId(),
                    log.getFeature(),
                    log.getType(),
                        log.getTekst()
                });
            }
            log.debug("End method.");
        } catch (Exception e) {
            log.debug(e.getMessage());
        }

        return model;
    }


    public void jButtonUpdateActionPerformed(ActionEvent ev) {
        log.debug("ActionEvent on " + ev.getActionCommand());
        // refresh the Table Model :
        jTable1.setModel(this.getData(this.loginBean));
    }

    public void jButtonResetActionPerformed(ActionEvent ev)
    {
        log.debug("ActionEvent on " + ev.getActionCommand());

        TransferRepository transferRepository = new TransferRepository();
        try {
            transferRepository.TransferReset(this.loginBean);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
