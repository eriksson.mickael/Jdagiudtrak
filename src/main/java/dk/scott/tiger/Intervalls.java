package dk.scott.tiger;

public class Intervalls {
    private int id;
    private String description;

    public Intervalls(int _id, String _description) {
        this.id = _id;
        this.description = _description;
    }
    public int getId() {
        return id;
    }
    public  String getDescription() {
        return description;
    }
}
