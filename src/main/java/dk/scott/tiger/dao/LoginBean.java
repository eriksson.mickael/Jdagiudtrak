package dk.scott.tiger.dao;

import java.util.prefs.Preferences;

public class LoginBean {
    private String tnsnames;
    private String userName;
    private String pwd;
    private int dbId;
    final Preferences prefs = Preferences.userNodeForPackage(dk.scott.tiger.Desktop.class);
    final String FIRST_TNS = "firstTns";
    final String SEC_TNS = "secondTns";
    final String FIRST_USER = "firstUser";
    final String FIRST_PWD = "firstPwd";
    final String SEC_USER = "sectUser";
    final String SEC_PWD = "secPwd";
    final String defaultValue = "";

    public String getTnsnames() {
        return tnsnames;
    }

    public void setTnsnames(String tnsnames) {
        this.tnsnames = tnsnames;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int _dbId) {
        this.dbId = _dbId;
    }
    /***
     *
     * @param _dbId
     * @return
     */
    public LoginBean get(int _dbId){
        LoginBean loginBean = new LoginBean();
    if(_dbId ==1){
        loginBean.setTnsnames(prefs.get(FIRST_TNS,""));
        loginBean.setUserName(prefs.get(FIRST_USER,""));
        loginBean.setPwd(prefs.get(FIRST_PWD, ""));
        loginBean.setDbId(_dbId);
    }else if(_dbId ==2){
        loginBean.setTnsnames( prefs.get(SEC_TNS,""));
        loginBean.setUserName( prefs.get(SEC_USER,""));
        loginBean.setPwd(prefs.get(SEC_PWD,""));
        loginBean.setDbId(_dbId);
    }
  return loginBean;
}
    /***
     *
     */
    public void save(){
        if(dbId ==1){
            prefs.put(FIRST_TNS,this.tnsnames);
            prefs.put(FIRST_USER,this.userName);
           prefs.put(FIRST_PWD,this.pwd);
        }else if(dbId ==2){
            prefs.put(SEC_TNS,this.tnsnames);
            prefs.put(SEC_USER,this.userName);
            prefs.put(SEC_PWD,this.pwd);
        }
    }
}
