/*
 * The MIT License
 *
 * Copyright 2017 Nafaa Friaa (nafaa.friaa@isetjb.rnu.tn).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dk.scott.tiger.dao;

import dk.scott.tiger.config.PROP;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DAOConnection
{
    final static Logger log = Logger.getLogger(DAOConnection.class);
    private static final String driver = PROP.getProperty("db.driver");
private static String user ;
  private static String password;
    private static Connection connection;
    private static final String tns =  System.getenv("TNS_ADMIN") ;
    /**
     * Private constructor so this class cannot be instantiated only by it self.
     */
    private DAOConnection()
    {
    }

    /**
     * Create and return the Connection if not exist.
     *
     * @return The connection object
     */
    public static Connection getInstance(LoginBean loginBean) throws SQLException {
        System.setProperty(
                "oracle.net.tns_admin",
                tns);
        String tnstest = System.getProperty(  "oracle.net.tns_admin");
        String pname = "property_" + loginBean.getDbId();
        String url = "jdbc:oracle:thin:@" + loginBean.getTnsnames();
        PoolDataSource pds = PoolDataSourceFactory.getPoolDataSource();
        MyConnectionLabelingCallback callback = new MyConnectionLabelingCallback();
        pds.registerConnectionLabelingCallback( callback );
        pds.setConnectionPoolName("JDBC_UCP");
        pds.setMinPoolSize(4);
        pds.setMaxPoolSize(20);
        Properties label = new Properties();
        label.setProperty(pname, loginBean.getTnsnames());

        try
        {
            pds.setUser(loginBean.getUserName());
            pds.setPassword(loginBean.getPwd());
            pds.setURL(url);
            pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
            connection =pds.getConnection (label);
           // if(connection.getMetaData().getConnection().getClientInfo().get(pname).toString().equals( loginBean.getTnsnames()))
             //        ((LabelableConnection) connection).applyConnectionLabel(pname, loginBean.getTnsnames());
        } catch (SQLException e)
        {
            log.error("DB Connection error : " + e.getMessage());
        }
    return connection;
}

}
