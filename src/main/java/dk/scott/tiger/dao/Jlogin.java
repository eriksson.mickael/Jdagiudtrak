package dk.scott.tiger.dao;

import dk.scott.tiger.config.I18N;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Jlogin extends JFrame implements ActionListener {
    JPanel panel;
    JLabel user_label, password_label, message, tns_lable;
    JTextField userName_text;
    JPasswordField password_text;
    JButton submit, cancel;
    JComboBox jComboBox;
    private LoginBean loginBean;
    private static final String tns =  System.getenv("TNS_ADMIN") + "/tnsnames.ora";
    private TNSUtil tnsUtil = new TNSUtil(tns);

    public Jlogin(LoginBean _loginBean) throws HeadlessException {
        this.setSize(new Dimension(300,150));
        this.setLocationRelativeTo(null);
        this.loginBean = _loginBean;

        // TNS
        jComboBox = new JComboBox(tnsUtil.getTNSentries().toArray());
        jComboBox.setSelectedItem(this.loginBean.getTnsnames().toLowerCase());
        tns_lable = new JLabel(I18N.lang("LoginForm.tns"));

        // Username
        user_label = new JLabel();
        user_label.setText(I18N.lang("LoginForm.user"));
        userName_text = new JTextField();
        userName_text.setText( this.loginBean.getUserName());

        // Password
        password_label = new JLabel();
        password_label.setText(I18N.lang("LoginForm.pwd"));
        password_text = new JPasswordField();
        password_text.setText(this.loginBean.getPwd());

        // Submit LoginForm.submit
        submit = new JButton(I18N.lang("LoginForm.submit"));

        // Panel
        panel = new JPanel(new GridLayout(4, 1));
        panel.add(tns_lable);
        panel.add(jComboBox);
        panel.add(user_label);
        panel.add(userName_text);
        panel.add(password_label);
        panel.add(password_text);

        message = new JLabel();
        panel.add(message);
        panel.add(submit);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Adding the listeners to components..
        submit.addActionListener(this);
        add(panel, BorderLayout.CENTER);
        setTitle(I18N.lang("LoginForm.Title"));
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String tns = jComboBox.getSelectedItem().toString();
        String userName = userName_text.getText();
        String password = new String(password_text.getPassword());
        loginBean.setTnsnames(tns);
        loginBean.setUserName(userName);
        loginBean.setPwd(password);
        loginBean.save();
    }
}
