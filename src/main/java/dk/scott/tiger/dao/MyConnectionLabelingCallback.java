package dk.scott.tiger.dao;

import dk.scott.tiger.Desktop;
import oracle.ucp.ConnectionLabelingCallback;
import oracle.ucp.jdbc.LabelableConnection;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class MyConnectionLabelingCallback implements ConnectionLabelingCallback {
    final static Logger log = Logger.getLogger(Desktop.class);
    @Override
    public int cost(Properties reqLabels, Properties currentLabels) {
        // Case 1: exact match
        if (reqLabels.equals(currentLabels))
        {
            log.debug("## Exact match found!! ##");
            return 0;
        }

        // Case 2: some labels match with no unmatched labels
        String iso1 = (String) reqLabels.get("TRANSACTION_ISOLATION");
        String iso2 = (String) currentLabels.get("TRANSACTION_ISOLATION");
        boolean match =
                (iso1 != null && iso2 != null && iso1.equalsIgnoreCase(iso2));
        Set rKeys = reqLabels.keySet();
        Set cKeys = currentLabels.keySet();
        if (match && rKeys.containsAll(cKeys))
        {
            log.debug("## Partial match found!! ##");
            return 10;
        }

        // No label matches to application's preference.
        // Do not choose this connection.
        log.debug("## No match found!! ##");
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean configure(Properties reqLabels, Object conn) {
        try
        {
            String isoStr = (String) reqLabels.get("TRANSACTION_ISOLATION");
            ((Connection)conn).setTransactionIsolation(Integer.valueOf(isoStr));
            LabelableConnection lconn = (LabelableConnection) conn;

            // Find the unmatched labels on this connection
            Properties unmatchedLabels =
                    lconn.getUnmatchedConnectionLabels(reqLabels);

            // Apply each label <key,value> in unmatchedLabels to conn
            for (Map.Entry<Object, Object> label : unmatchedLabels.entrySet())
            {
                String key = (String) label.getKey();
                String value = (String) label.getValue();
                lconn.applyConnectionLabel(key, value);
            }
        }
        catch (Exception exc)
        {
            return false;
        }
        return true;
    }
}
