package dk.scott.tiger.dao;

import edu.gatech.gtri.orafile.Orafile;
import edu.gatech.gtri.orafile.OrafileDef;
import edu.gatech.gtri.orafile.OrafileDict;
import edu.gatech.gtri.orafile.OrafileVal;
import org.antlr.runtime.RecognitionException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import java.util.stream.Stream;

public class TNSUtil {
    final String tnsFileContent;
    final static Logger log = Logger.getLogger(TNSUtil.class);
    final Preferences prefs = Preferences.userNodeForPackage(dk.scott.tiger.Desktop.class);
    final String FIRST_TNS = "firstTns";
    final String SEC_TNS = "secondTns";
    final String FIRST_USER = "firstUser";
    final String FIRST_PWD = "firstPwd";
    final String SEC_USER = "sectUser";
    final String SEC_PWD = "secPwd";
    final String defaultValue = "";
    public TNSUtil(String filePath) {
        log.debug("START constructor..." );
        this.tnsFileContent = readLineByLineJava8(filePath);
        log.debug("STOP constructor..." );
    }
    private  String readLineByLineJava8(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }
    public List<String> getTNSentries() {
        List<String> strings = null;
        try {
            strings = new ArrayList<>();
            // Parse the string.
            OrafileDict tns = Orafile.parse(this.tnsFileContent);
            List<OrafileDef> orafileVals=  tns.asList();
            for(int i =0;i < orafileVals.size(); i++ ){
                strings.add( orafileVals.get(i).getName());
            }
        } catch (RecognitionException e) {
            log.debug("ERROR..." + e.getMessage());
            e.printStackTrace();
        }catch (Exception e){
            log.debug("ERROR..." + e.getMessage());
        }
        finally {
            return strings;
        }
    }

    public List<Map<String, String>> getTNSentry(String entry) throws RecognitionException {
       OrafileDict tns = Orafile.parse(this.tnsFileContent);
       OrafileVal val = tns.get(entry).get(0);
       return val
              .findParamAttrs("address", Arrays.asList("host", "port", "sid"));
    }

    public LoginBean getLogin(int dbId){
        LoginBean loginBean = new LoginBean();
       return  loginBean.get(dbId);
    }
}
