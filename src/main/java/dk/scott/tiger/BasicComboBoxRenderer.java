package dk.scott.tiger;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;

class ItemRenderer extends BasicComboBoxRenderer {
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected,
                cellHasFocus);

        if (value != null) {
            Intervalls item = (Intervalls) value;
            setText(item.getDescription().toUpperCase());
        }

        if (index == -1) {
            Intervalls item = (Intervalls) value;
            setText("" + item.getDescription());
        }

        return this;
    }
}