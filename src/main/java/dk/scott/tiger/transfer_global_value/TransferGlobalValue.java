package dk.scott.tiger.transfer_global_value;

import javax.persistence.*;

@Entity
@Table(name = "TRANSFER_GLOBAL_VALUE")
public class TransferGlobalValue {
    @Column(name = "X")
    private Long x;



    public Long getX() {
       return this.x;

   }
    public void setX(Long x) {
        this.x = x;
    }
}
