package dk.scott.tiger.transfer_global_value;

import dk.scott.tiger.dao.DAOConnection;
import dk.scott.tiger.dao.LoginBean;
import dk.scott.tiger.dao.Repository;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class TransferRepository implements Repository<TransferGlobalValue> {
    final static Logger log = Logger.getLogger(TransferRepository.class);
    @Override
    public TransferGlobalValue find(long id, LoginBean tns) throws SQLException {
        TransferGlobalValue transferGlobalValue = new TransferGlobalValue();
        PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement("select * from  transfer_global_value" );
        ResultSet resultSet = prepared.executeQuery();
        while (resultSet.next())
        {
        transferGlobalValue.setX(resultSet.getLong(1));
        }
        prepared.close();
        prepared=null;
        return transferGlobalValue;
    }

    @Override
    public TransferGlobalValue find(Timestamp id, LoginBean tns) {
        return null;
    }

    @Override
    public ArrayList<TransferGlobalValue> findAll(LoginBean tns) {
        return null;
    }

    @Override
    public ArrayList<TransferGlobalValue> findFirst(LoginBean tns, int count) {
        return null;
    }

    public void TransferReset(LoginBean tns) throws SQLException {
        PreparedStatement prepared = DAOConnection.getInstance(tns).prepareStatement("update transfer_global_value set x=0");
        try {
            prepared.executeQuery();
            prepared.close();
            prepared=null;
        } catch (SQLException e) {
            log.error("Error finding products : " + e);
        }
    }
}
