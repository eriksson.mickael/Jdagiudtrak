package dk.scott.tiger;

public class Constants {
    public Constants() {
    }
    public static final int oneSecond = 1000;
    public static final int twoSeconds = 10000;
    public static final int oneMinute = 60000;
    public static final int twoMinutes = 120000;
}
